# Imports
from unittest import main, TestCase
from Maximum import maximum

class MyUnitTests (TestCase):
    def test_maximum (self):
        self.assertEqual(maximum(1, 6, 3), 6)

if __name__ == "__main__":
    main()